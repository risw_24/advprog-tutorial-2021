package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {

    public String defend(){
        return "kaga mundur boi";
    }

    public String getType(){
        return "Pakai perisai";
    }
}
