package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {

    public String attack(){
        return "dor dor";
    }
    public String getType(){
        return "Pakai pistol";
    }
}
