package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {


    public String attack(){
        return "simsalabim";
    }

    public String getType(){
        return "Pakai sihir";
    }
}
