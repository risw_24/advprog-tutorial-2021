package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {


    public String defend(){
        return "tahan banting";
    }

    public String getType(){
        return "Pakai baju Armor";
    }
}
