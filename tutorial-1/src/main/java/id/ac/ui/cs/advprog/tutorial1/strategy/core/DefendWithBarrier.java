package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    

    public String defend(){
        return "tetap kokoh";
    }

    public String getType(){
        return "Pakai barrier";
    }
}
