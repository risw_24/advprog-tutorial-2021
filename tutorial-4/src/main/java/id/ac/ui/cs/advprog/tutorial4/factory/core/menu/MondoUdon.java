package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory;

public class MondoUdon extends Menu {
    //Ingridients:
    //Noodle: Udon
    //Meat: Chicken
    //Topping: Cheese
    //Flavor: Salty
    private MenuFactory menuFactory;
    public MondoUdon(String name, MenuFactory menuFactory){
        super(name);
        this.menuFactory = menuFactory;
        create();
    }

    @Override
    void create() {
        setFlavor(menuFactory.makeFlavor());
        setMeat(menuFactory.makeMeat());
        setNoodle(menuFactory.makeNoodle());
        setTopping(menuFactory.makeTopping());
    }
}