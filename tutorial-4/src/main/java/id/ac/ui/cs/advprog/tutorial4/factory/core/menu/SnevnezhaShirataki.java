package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory;

public class SnevnezhaShirataki extends Menu {
    //Ingridients:
    //Noodle: Shirataki
    //Meat: Fish
    //Topping: Flower
    //Flavor: Umami
    private MenuFactory menuFactory;
    public SnevnezhaShirataki(String name, MenuFactory menuFactory){
        super(name);
        this.menuFactory = menuFactory;
        create();
    }

    @Override
    void create() {
        setMeat(menuFactory.makeMeat());
        setFlavor(menuFactory.makeFlavor());
        setNoodle(menuFactory.makeNoodle());
        setTopping(menuFactory.makeTopping());
    }
}