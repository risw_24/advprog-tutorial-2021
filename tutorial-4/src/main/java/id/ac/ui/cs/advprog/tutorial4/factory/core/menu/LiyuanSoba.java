package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory;

public class LiyuanSoba extends Menu {
    //Ingridients:
    //Noodle: Soba
    //Meat: Beef
    //Topping: Mushroom
    //Flavor: Sweet
    private MenuFactory menuFactory;
    public LiyuanSoba(String name,MenuFactory menuFactory){
        super(name);
        this.menuFactory = menuFactory;
        create();
    }

    @Override
    void create(){
        setFlavor(menuFactory.makeFlavor());
        setMeat(menuFactory.makeMeat());
        setNoodle(menuFactory.makeNoodle());
        setTopping(menuFactory.makeTopping());
    }
}