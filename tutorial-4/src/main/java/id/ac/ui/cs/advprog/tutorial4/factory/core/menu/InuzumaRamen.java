package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory;

public class InuzumaRamen extends Menu {
    //Ingridients:
    //Noodle: Ramen
    //Meat: Pork
    //Topping: Boiled Egg
    //Flavor: Spicy
    private MenuFactory menuFactory;

    public InuzumaRamen(String name,MenuFactory menuFactory){
        super(name);
        this.menuFactory = menuFactory;
        create();
    }

    @Override
    void create() {
        setNoodle(menuFactory.makeNoodle());
        setFlavor(menuFactory.makeFlavor());
        setMeat(menuFactory.makeMeat());
        setTopping(menuFactory.makeTopping());
    }
}