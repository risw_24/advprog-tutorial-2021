package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MenuRepositoryTest {
    private Class<?> menurepoClass;
    private Noodle noodle;
    private Meat meat;
    private Topping topping;
    private Flavor flavor;
    @BeforeEach
    void setUp() throws Exception{
        menurepoClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository");
    }

    @Test
    void getMenus() {
        noodle = new Ramen();
        meat = new Fish();
        topping = new Cheese();
        flavor = new Salty();
        MenuRepository menus = new MenuRepository();
        //Menu ramen = new Menu("ramen",noodle,meat,topping,flavor);

        //assertEquals(menus.add(ramen),ramen);
        //assertEquals(menus.getMenus(),);
    }
}