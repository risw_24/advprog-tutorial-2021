package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FishTest {
    private Class<?> fishClass;
    @BeforeEach
    void setUp() throws Exception{
        fishClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish");
    }

    @Test
    void FishgetDescription() {
        Fish fish = new Fish();
        assertEquals(fish.getDescription(),"Adding Zhangyun Salmon Fish Meat...");
    }
}