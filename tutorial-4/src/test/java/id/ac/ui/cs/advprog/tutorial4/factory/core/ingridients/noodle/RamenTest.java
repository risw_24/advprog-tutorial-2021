package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RamenTest {
    private Class<?> ramenClass;
    @BeforeEach
    void setUp() throws Exception{
        ramenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen");
    }

    @Test
    void RamengetDescription() {
        Ramen ramen = new Ramen();
        assertEquals(ramen.getDescription(),"Adding Inuzuma Ramen Noodles...");
    }
}