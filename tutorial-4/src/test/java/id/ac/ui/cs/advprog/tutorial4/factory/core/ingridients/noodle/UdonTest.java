package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UdonTest {
    private Class<?> udonClass;
    @BeforeEach
    void setUp() throws Exception{
        udonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon");
    }

    @Test
    void UdongetDescription() {
        Udon udon = new Udon();
        assertEquals(udon.getDescription(),"Adding Mondo Udon Noodles...");
    }
}