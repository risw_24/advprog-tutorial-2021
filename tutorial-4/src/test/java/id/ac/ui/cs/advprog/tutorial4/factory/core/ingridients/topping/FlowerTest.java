package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FlowerTest {
    private Class<?> flowerClass;
    @BeforeEach
    void setUp() throws Exception{
        flowerClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower");
    }

    @Test
    void FlowergetDescription() {
        Flower flower = new Flower();
        assertEquals(flower.getDescription(),"Adding Xinqin Flower Topping...");
    }
}