package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ShiratakiTest {
    private Class<?> shiratakiClass;
    @BeforeEach
    void setUp() throws Exception{
        shiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki");
    }

    @Test
    void ShiratakigetDescription() {
        Shirataki shirataki = new Shirataki();
        assertEquals(shirataki.getDescription(),"Adding Snevnezha Shirataki Noodles...");
    }
}