package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ChickenTest {
    private Class<?> chickenClass;
    @BeforeEach
    void setUp() throws Exception{
        chickenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken");
    }

    @Test
    void ChickengetDescription() {
        Chicken chicken = new Chicken();
        assertEquals(chicken.getDescription(),"Adding Wintervale Chicken Meat...");
    }
}