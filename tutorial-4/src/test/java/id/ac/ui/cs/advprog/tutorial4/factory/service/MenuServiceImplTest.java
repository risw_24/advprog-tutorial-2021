package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MenuServiceImplTest {
    private Class<?> menuimplClass;
    private Noodle noodle;
    private Meat meat;
    private Topping topping;
    private Flavor flavor;
    @BeforeEach
    void setUp() throws Exception{
        menuimplClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
    }

    @Test
    void createMenu() {
        MenuServiceImpl menuService = new MenuServiceImpl();
        menuService.createMenu("WanPlus Beef Mushroom Soba", "LiyuanSoba");
        MenuRepository menuRepository = new MenuRepository();
        //menuRepository.add(menuService.createMenu("WanPlus Beef Mushroom Soba", "LiyuanSoba"));
        List<Menu> menus = new ArrayList<>();
        menus.add(menuRepository.add(menuService.createMenu("WanPlus Beef Mushroom Soba", "LiyuanSoba")));
        //assertEquals(menuRepository.getMenus(),menuService.createMenu("WanPlus Beef Mushroom Soba", "LiyuanSoba");
    }
}