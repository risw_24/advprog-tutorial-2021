package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BoiledEggTest {
    private Class<?> boiledeggClass;
    @BeforeEach
    void setUp() throws Exception{
        boiledeggClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg");
    }

    @Test
    void BoiledEgggetDescription() {
        BoiledEgg boiledEgg = new BoiledEgg();
        assertEquals(boiledEgg.getDescription(),"Adding Guahuan Boiled Egg Topping");
    }
}