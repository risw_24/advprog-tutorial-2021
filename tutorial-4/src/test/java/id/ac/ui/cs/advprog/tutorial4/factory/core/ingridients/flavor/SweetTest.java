package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SweetTest {
    private Class<?> sweetClass;
    @BeforeEach
    void setUp() throws Exception{
        sweetClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet");
    }

    @Test
    void SweetgetDescription() {
        Sweet sweet = new Sweet();
        assertEquals(sweet.getDescription(),"Adding a dash of Sweet Soy Sauce...");
    }
}