package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BeefTest {
    private Class<?> beefClass;
    @BeforeEach
    void setUp() throws Exception{
        beefClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef");
    }

    @Test
    void BeefgetDescription() {
        Beef beef = new Beef();
        assertEquals(beef.getDescription(),"Adding Maro Beef Meat...");
    }
}