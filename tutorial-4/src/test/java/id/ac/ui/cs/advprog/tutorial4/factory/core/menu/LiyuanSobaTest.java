package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LiyuanSobaTest {
    private Class<?> liyuanSobaClass;
    @BeforeEach
    void setUp() throws Exception{
        liyuanSobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
    }

    @Test
    void LiyuanSobagetName() {
        LiyuanSoba liyuanSoba = new LiyuanSoba("soba",new LiyuanSobaFactory());
        assertEquals(liyuanSoba.getName(),"soba");
    }
}