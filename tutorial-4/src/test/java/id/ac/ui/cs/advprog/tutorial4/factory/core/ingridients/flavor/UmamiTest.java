package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UmamiTest {

    private Class<?> umamiClass;
    @BeforeEach
    void setUp() throws Exception{
        umamiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami");
    }

    @Test
    void UmamigetDescription() {
        Umami umami = new Umami();
        assertEquals(umami.getDescription(),"Adding WanPlus Specialty MSG flavoring...");
    }
}