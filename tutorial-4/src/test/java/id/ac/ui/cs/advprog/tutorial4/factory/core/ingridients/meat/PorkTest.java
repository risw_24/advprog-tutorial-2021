package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PorkTest {
    private Class<?> porkClass;
    @BeforeEach
    void setUp() throws Exception{
        porkClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork");
    }

    @Test
    void PorkgetDescription() {
        Pork pork = new Pork();
        assertEquals(pork.getDescription(),"Adding Tian Xu Pork Meat...");
    }
}