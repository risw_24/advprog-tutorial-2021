package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SaltyTest {
    private Class<?> saltyClass;
    @BeforeEach
    void setUp() throws Exception{
        saltyClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty");
    }

    @Test
    void SaltygetDescription() {
        Salty salty = new Salty();
        assertEquals(salty.getDescription(),"Adding a pinch of salt...");
    }
}