package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SobaTest {
    private Class<?> sobaClass;
    @BeforeEach
    void setUp() throws Exception{
        sobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba");
    }

    @Test
    void SobagetDescription() {
        Soba soba = new Soba();
        assertEquals(soba.getDescription(),"Adding Liyuan Soba Noodles...");
    }
}