package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MushroomTest {
    private Class<?> mushroomClass;
    @BeforeEach
    void setUp() throws Exception{
        mushroomClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom");
    }

    @Test
    void MushroomgetDescription() {
        Mushroom mushroom = new Mushroom();
        assertEquals(mushroom.getDescription(),"Adding Shiitake Mushroom Topping...");
    }
}