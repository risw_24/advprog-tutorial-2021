package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SnevnezhaShiratakiTest {
    private Class<?> shiratakiClass;
    @BeforeEach
    void setUp() throws Exception{
        shiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");
    }

    @Test
    void ShiratakiName() {
        SnevnezhaShirataki shirataki = new SnevnezhaShirataki("shirataki",new SnevnezhaShiratakiFactory());
        assertEquals(shirataki.getName(),"shirataki");
    }
}