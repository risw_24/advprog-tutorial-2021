package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MondoUdonTest {
    private Class<?> mondoUdonClass;
    @BeforeEach
    void setUp() throws Exception{
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
    }

    @Test
    void MondoUdongetName() {
        MondoUdon mondoUdon = new MondoUdon("udon",new MondoUdonFactory());
        assertEquals(mondoUdon.getName(),"udon");
    }
}