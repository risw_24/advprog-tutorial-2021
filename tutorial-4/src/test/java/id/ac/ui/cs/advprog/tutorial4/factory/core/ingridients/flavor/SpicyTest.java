package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SpicyTest {
    private Class<?> spicyClass;
    @BeforeEach
    void setUp() throws Exception{
        spicyClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy");
    }

    @Test
    void SpicygetDescription() {
        Spicy spicy = new Spicy();
        assertEquals(spicy.getDescription(),"Adding Liyuan Chili Powder...");
    }
}