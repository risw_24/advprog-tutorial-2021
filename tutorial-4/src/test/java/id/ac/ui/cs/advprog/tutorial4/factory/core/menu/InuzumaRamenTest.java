package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InuzumaRamenTest {
    private Class<?> inuzumaRamenClass;
    @BeforeEach
    void setUp() throws Exception{
        inuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
    }

    @Test
    void InuzumaRamengetName() {
        InuzumaRamen inuzumaRamen = new InuzumaRamen("ramen", new InuzumaRamenFactory());
        assertEquals(inuzumaRamen.getName(),"ramen");
    }
}