package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CheeseTest {
    private Class<?> cheeseClass;
    @BeforeEach
    void setUp() throws Exception{
        cheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese");
    }

    @Test
    void CheesegetDescription() {
        Cheese cheese = new Cheese();
        assertEquals(cheese.getDescription(),"Adding Shredded Cheese Topping...");
    }
}