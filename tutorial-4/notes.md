###Eager Instantiation
private static OrderFood orderFood = new OrderFood();

public static OrderFood getInstance() {
return orderFood;
}

Implementasi instansiasi ini seperti code diatas. Instansiasi ini akan langsung membuat instansiasi walaupun tidak digunakan.

Kelebihan
1. Langsung membuat instansiasi kelas sehingga hanya dipastikan satu instansiasi saja yang dipanggil

Kekurangan
1. Akan tetap terpanggil walaupun tidak dibutuhkan sehingga membuat boros
   

###Lazy Instantiation
private static OrderDrink orderDrink = null;

public static OrderDrink getInstance() {
        if(orderDrink==null){
            orderDrink = new OrderDrink();
        }
        return orderDrink;
    }

Implementasi inisiasi ini seperti code di atas. Pada jenis ini akan dicek terlebih dahulu apakah sudah diinisiasi atau belum.

Kelebihan
1. Class tidak akan diinisiasi sampai dipanggil satu kali. Jika sudah dipanggil, maka tidak akan memanggil lagi

Kekurangan
1. Tidak cocok jika class diakses oleh banyak request, sehingga jika dipanggil secara bersamaan bisa menimbulkan error