package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

public class PlusOneTransformation {


    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        int selector = encode ? 1 : -1;
        String text = spell.getText();
        Codex codex = spell.getCodex();
        char[] res = new char[text.length()];
        for(int i = 0; i < res.length; i++){
            char oldChar = text.charAt(i);
            int charIdx = codex.getIndex(oldChar);
            int newCharIdx = charIdx+selector;
            if(newCharIdx==text.length()){
                newCharIdx = 0;
            }else if(newCharIdx<0){
                newCharIdx = text.length()-1;
            }
            res[i] = codex.getChar(newCharIdx);
        }
        return new Spell(new String(res), codex);
    }
}
