package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private LogRepository logRepository;
    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;

    private Weapon weapon;
    private String last;


    public WeaponServiceImpl() {
        this.weaponRepository = new WeaponRepositoryImpl();
        this.logRepository = new LogRepositoryImpl();
        this.bowRepository = new BowRepositoryImpl();
        this.spellbookRepository = new SpellbookRepositoryImpl();
    }
    public void lastCall(String weapon){
        last = weapon;
    }

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {

        for(Bow bow:bowRepository.findAll()){
            weaponRepository.save(new BowAdapter(bow));
        }
        for(Spellbook spellbook :spellbookRepository.findAll()){
            weaponRepository.save(new SpellbookAdapter(spellbook));
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        weapon = weaponRepository.findByAlias(weaponName);
        if(attackType==0) {
            weapon.normalAttack();
            weaponRepository.save(weapon);
            logRepository.addLog(weapon.getHolderName()+" attacked with " + weapon.getName() + "(normal attack):"+ weapon.normalAttack());
            last = null;
        }
        else if(attackType==1){
            if(weapon.getType().equals("Spellbook")) {
                if (!(weapon.getHolderName()).equals(last)) {
                    weapon.chargedAttack();
                    weaponRepository.save(weapon);
                    logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() + "(charged attack):" + weapon.chargedAttack());
                    lastCall(weapon.getHolderName());
                }
            }else{
                weapon.chargedAttack();
                weaponRepository.save(weapon);
                logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() + "(charged attack):" + weapon.chargedAttack());
                lastCall(weapon.getHolderName());
            }
        }
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
