package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PlusOneTransformationTest {
    private Class<?> plusoneclass;

    @BeforeEach
    public void setup() throws Exception {
        plusoneclass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.PlusOneTransformation");
    }

    @Test
    public void testPlusOneHasEncodeMethod() throws Exception {
        Method translate = plusoneclass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testPlusOneEncodesCorrectly() throws Exception {
        String text = "Halo";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Ibmp";

        Spell result = new PlusOneTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }



    @Test
    public void testPlusOneHasDecodeMethod() throws Exception {
        Method translate = plusoneclass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testPlusOneDecodesCorrectly() throws Exception {
        String text = "Halo";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "GZkn";

        Spell result = new PlusOneTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

}
