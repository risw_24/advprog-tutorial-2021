package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
@Data
@NoArgsConstructor
public class LogRingkasan {
    @Autowired
    private String bulan;
    @Autowired
    private int tahun;
    @Autowired
    private double waktuKerja;
    @Autowired
    private double bayaran;

    public void addKerja(double waktuKerja){
        this.waktuKerja += waktuKerja;
        this.bayaran = this.waktuKerja*350;
    }

    public LogRingkasan(String bulan,int tahun, double waktuKerja){
        this.bulan = bulan;
        this.tahun = tahun;
        this.waktuKerja = waktuKerja;
        this.bayaran = waktuKerja*350;
    }

}
