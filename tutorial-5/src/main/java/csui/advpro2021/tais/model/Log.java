package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {

    @Id
    @Column(name = "number", updatable = false, nullable = false)
    private String number;

    @Column(name = "deskripsi")
    private String deskripsi;

    @Column(name = "waktu")
    private double waktu;

    @Column(name = "mulai")
    private LocalDateTime mulai;

    @Column(name = "akhir")
    private LocalDateTime akhir;

    @ManyToOne
    @JoinColumn(name="FK_asdos")
    private Mahasiswa mahasiswa;

    public Log(String number,String deskripsi, String mulai, String akhir) {
        this.number = number;
        this.deskripsi = deskripsi;
        this.mulai = LocalDateTime.parse(mulai);
        this.akhir = LocalDateTime.parse(akhir);
        this.waktu = this.mulai.until(this.akhir, ChronoUnit.SECONDS)/3600;
    }
}
