package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogRingkasan;

public interface LogService {
    Log createLog(Log log,String npm);

    Log getLogByNumber(String number);

    Log updateLog(String number, Log log);

    void deleteLogByNumber(String number);

    Iterable<Log> getLogByMahasiswa(String npm, String bulan);

    Iterable<LogRingkasan> getRingkasanLog(String npm);

    //Log masukLog(String npm,Log log);
}
