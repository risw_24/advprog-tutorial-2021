package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MahasiswaServiceImpl implements MahasiswaService {
    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private MataKuliahService mataKuliahService;

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Override
    public Mahasiswa createMahasiswa(Mahasiswa mahasiswa) {
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public Iterable<Mahasiswa> getListMahasiswa() {
        return mahasiswaRepository.findAll();
    }

    @Override
    public Mahasiswa getMahasiswaByNPM(String npm) {
        //System.out.println(mahasiswaRepository.findByNpm(npm));
        return mahasiswaRepository.findByNpm(npm);
    }

    @Override
    public Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa) {
        mahasiswa.setNpm(npm);
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public Mahasiswa daftarAsdos(String npm, String kode_matkul) {
        MataKuliah mataKuliah = mataKuliahService.getMataKuliah(kode_matkul);
        Mahasiswa mahasiswa = getMahasiswaByNPM(npm);

        mahasiswa.setMatakuliah(mataKuliah);
        List<Mahasiswa> mahasiswa_list = mataKuliah.getAsdos();
        mahasiswa_list.add(mahasiswa);
        mataKuliah.setAsdos(mahasiswa_list);
        mahasiswaRepository.save(mahasiswa);
        mataKuliahRepository.save(mataKuliah);

        return null;
    }

    @Override
    public void deleteMahasiswaByNPM(String npm) {
        mahasiswaRepository.deleteById(npm);
    }

    @Override
    public MataKuliah getMataKuliah(String npm) {
        Mahasiswa mahasiswa = getMahasiswaByNPM(npm);
        return mahasiswa.getMatakuliah();
    }
}
