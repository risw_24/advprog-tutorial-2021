package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogRingkasan;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormatSymbols;
import java.util.*;

@Service
public class LogServiceImpl implements LogService{
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Override
    public Log createLog(Log log,String npm) {
        log.setMahasiswa(mahasiswaService.getMahasiswaByNPM(npm));
        logRepository.save(log);

        List<Log> log_list = mahasiswaService.getMahasiswaByNPM(npm).getDaftar_log();
        log_list.add(log);
        mahasiswaService.getMahasiswaByNPM(npm).setDaftar_log(log_list);
        mahasiswaRepository.save(mahasiswaService.getMahasiswaByNPM(npm));
        logRepository.save(log);
        return log;
    }

    @Override
    public Log getLogByNumber(String number) {
        return logRepository.findByNumber(number);
    }

    @Override
    public Log updateLog(String number, Log log) {
        log.setNumber(number);
        logRepository.save(log);
        return log;
    }

    @Override
    public void deleteLogByNumber(String number) {
        logRepository.deleteById(number);
    }



    @Override
    public Iterable<Log> getLogByMahasiswa(String npm, String bulan) {

        List<Log> logs = mahasiswaService.getMahasiswaByNPM(npm).getDaftar_log();
        System.out.println(logs.isEmpty());
        List<Log> result = new ArrayList();
        if(logs.isEmpty()){
            return result;
        }
        for(Log log : logs){
            if(log.getMulai().getMonth().name().equals(bulan)){
                System.out.println("masuk");
                result.add(log);
            }
        }

        return result;
    }

    @Override
    public Iterable<LogRingkasan> getRingkasanLog(String npm) {
        System.out.println("masuk");
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        List<Log> logs = mahasiswa.getDaftar_log();
        List<LogRingkasan> ringkasan = new ArrayList();
        Map<String, LogRingkasan> dataBulanan = new HashMap<>();
        System.out.println("masuk lagi");
        for(Log log : logs){
            System.out.println("masuk lagi lagi");
            String bulan = log.getMulai().getMonth().name();
            int tahun = log.getMulai().getYear();
            LogRingkasan ringkasan_init = new LogRingkasan(bulan,tahun, log.getWaktu());
            LogRingkasan logRingkasan = dataBulanan.get(bulan);

            if(logRingkasan==null){
                dataBulanan.put(bulan, ringkasan_init);
            }else{
                logRingkasan.addKerja(log.getWaktu());
            }
        }
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        for(String month:months){
            LogRingkasan data = dataBulanan.get(month.toUpperCase(Locale.ROOT));
            ringkasan.add(data);
        }
        System.out.println("masuk lagi lagi lagi");
        return ringkasan;
    }
}
