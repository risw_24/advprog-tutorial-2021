package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/log")
public class LogController {

    @Autowired
    private LogService logService;

    @PostMapping(path = "/{npm}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postLog(@PathVariable(value = "npm") String npm,@RequestBody Log log) {
        return ResponseEntity.ok(logService.createLog(log,npm));
    }

    @GetMapping(path = "/{number}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity geLogByNumber(@PathVariable(value = "number") String number) {
        Log log = logService.getLogByNumber(number);
        if (log == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(log);
    }

    @PutMapping(path = "/{number}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@PathVariable(value = "number") String number, @RequestBody Log log) {
        return ResponseEntity.ok(logService.updateLog(number, log));
    }

    @DeleteMapping(path = "/{number}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "number") String number) {
        logService.deleteLogByNumber(number);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/{npm}/{bulan}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity geLogByMahasiswa(@PathVariable(value = "npm") String npm ,@PathVariable(value = "bulan") String bulan) {
        return ResponseEntity.ok(logService.getLogByMahasiswa(npm,bulan));
    }

    @GetMapping(path = "/{npm}/ringkasan", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getRingkasanLog(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getRingkasanLog(npm));
    }
}
