package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;
    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @InjectMocks
    private LogServiceImpl logService;
    @InjectMocks
    private MahasiswaServiceImpl mahasiswaService;


    private Log log;
    private Mahasiswa mahasiswa;
    @BeforeEach
    void setUp() {
        mahasiswa = new Mahasiswa("1906293171", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");


        log = new Log();
        log.setMulai(LocalDateTime.parse("2021-01-01T10:00:00.00"));
        log.setAkhir(LocalDateTime.parse("2021-01-01T11:00:00.00"));
        log.setDeskripsi("belajar");
        log.setNumber("1");
        log.setWaktu(60);
        log.setMahasiswa(mahasiswa);
    }

    @Test
    void createLog() {

    }

    @Test
    void getLogByNumber() {
        lenient().when(logService.getLogByNumber("1")).thenReturn(log);
        Log resultLog = logService.getLogByNumber(log.getNumber());
        assertEquals(log.getNumber(),resultLog.getNumber());
    }

    @Test
    void updateLog() {
    }

    @Test
    void deleteLogByNumber() {

    }

    @Test
    void getLogByMahasiswa() {
    }

    @Test
    void getRingkasanLog() {
    }
}